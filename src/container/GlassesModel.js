import React, { Component } from "react";
import GlassesDetail from "./GlassesDetail";
import styles from "./GlassesModel.module.css";

export default class GlassesModel extends Component {
  render() {
    let currentGlasses = this.props.currentGlasses;
    return (
      <div className={styles.GlassesModel}>
        <img
          className="img-fluid w-100 rounded-5 border"
          src="./img/model.jpg"
          alt=""
        />

        {Object.keys(currentGlasses).length !== 0 ? (
          <>
            <img
              className={styles.vGlasses}
              src={currentGlasses.virtualImg}
              alt=""
            />
            <GlassesDetail currentGlasses={currentGlasses} />
          </>
        ) : null}
      </div>
    );
  }
}
