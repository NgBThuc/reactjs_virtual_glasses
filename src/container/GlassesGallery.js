/* eslint-disable jsx-a11y/alt-text */
import React, { Component } from "react";
import GlassesItem from "./GlassesItem";

export default class GlassesGallery extends Component {
  render() {
    return (
      <div>
        <h1>Virtual Glasses</h1>
        <div className="row">
          {this.props.dataGlasses.map((item) => {
            return (
              <GlassesItem
                handleClickGlasses={this.props.handleClickGlasses}
                key={item.id}
                item={item}
              />
            );
          })}
        </div>
      </div>
    );
  }
}
