import React, { Component } from "react";
import styles from "./GlassesDetail.module.css";

export default class GlassesDetail extends Component {
  render() {
    let currentGlasses = this.props.currentGlasses;
    return (
      <div className={`p-3 rounded-5 ${styles.GlassesDetail}`}>
        <h2 className={styles.heading}>
          {currentGlasses.name.toUpperCase()} - {currentGlasses.brand} (
          {currentGlasses.color})
        </h2>
        <div className="my-2">
          <span className={`me-2 ${styles.price}`}>
            ${currentGlasses.price}
          </span>
          <span className={styles.stock}>Stocking</span>
        </div>
        <p>{currentGlasses.description}</p>
      </div>
    );
  }
}
