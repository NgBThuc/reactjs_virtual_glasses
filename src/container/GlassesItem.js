import React, { Component } from "react";
import styles from "./GlassesItem.module.css";

export default class GlassesItem extends Component {
  render() {
    return (
      <div
        onClick={() => {
          this.props.handleClickGlasses(this.props.item.id);
        }}
        className={`col-6 col-lg-4 d-flex align-items-center ${styles.GlassItem}`}
      >
        <img className="img-fluid" src={this.props.item.src} alt="" />
      </div>
    );
  }
}
