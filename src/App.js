import React, { Component } from "react";
import "./App.css";
import GlassesGallery from "./container/GlassesGallery";
import GlassesModel from "./container/GlassesModel";
import { dataGlasses } from "./data/dataGlasses";

export default class App extends Component {
  state = {
    dataGlasses: dataGlasses,
    currentGlasses: {},
  };

  handleClickGlasses = (id) => {
    this.setState({
      currentGlasses: this.state.dataGlasses.find((item) => item.id === id),
    });
  };

  render() {
    return (
      <div className="App">
        <div className="container py-5">
          <div className="row">
            <div className="col-12 col-lg-6">
              <GlassesGallery
                handleClickGlasses={this.handleClickGlasses}
                dataGlasses={this.state.dataGlasses}
              />
            </div>
            <div className="col-12 col-lg-6">
              <GlassesModel currentGlasses={this.state.currentGlasses} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
